### Selenium drivers required to run the code below ###
# 필수 셀레니엄 드라이버
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import requests
import bs4
from bs4 import BeautifulSoup
from selenium.webdriver.common.by import By
import time
from selenium.webdriver import ActionChains
import threading
import time### Thread run function to run consistently ###
# Thread_run 함수 반복작업
def thread_run():
# print("====", time.ctime(), "====")      ## show time: 시간보여주기
    crawling()
    threading.Timer(3600, thread_run).start()  ## change this '10' seconds to re-start running the code: 시간 정해서 반복문 돌리기
## For example, change it to '3600' as an hour# def start_crawling():                        ## run start_crwaling function: 함수 구현으로 돌리기
def crawling():
    driver = webdriver.Chrome()  ## have to install chromedriver, and the PATH where the driver is
    #: Chromedriver 설치 후 해당 PC 맞는 곳 적기
    driver.implicitly_wait(3)  ## Wait implicitly 3 seconds: 3초 최대 기다리기
    # upload the website: 웹사이트 업로드
    driver.get('https://eadvantage.siemens.com/uaa/login')  ## using driver.get('') we can upload a web page
    # ok for the banner: 각 버튼 클릭
    driver.maximize_window()
    driver.find_element_by_xpath('//*[@id="banner-ok"]').click()  ## using driver.find_element_by_xpath().CLICK()
    ## press direct-to-login-page button                                 ## we can click on each icons or menubars, that we need
    driver.find_element_by_xpath(
        '//*[@class="login-form-button"]').click()  ## IMPORTANT PART is to find correct ids, and class names:
    # 아이디와 클래스 이름 찾기
    # # insert id and pw
    driver.find_element_by_name('email').send_keys('tamami@inu.ac.kr')
    driver.find_element_by_name('password').send_keys('1qaz!QAZ')
    # # press login button
    driver.find_element_by_xpath('//*[@class="auth0-lock-submit"]').click()
    driver.implicitly_wait(3000)
    # time.sleep(10)
    driver.switch_to.frame('dashboard-frame')
    # wait until next page is uploaded, otherwise, we cannot find next page
    # driver.implicitly_wait() 하지 않으면 페이지 로딩 시간 때문에 다음으로 넘어갈 수 없음
    # driver.execute_script("window.open('');")                             ## new window open: 새 윈도우 열기
    # Switch to the new window: 새 윈도우로 변경
    time.sleep(10)
    #-----------------driver.find_element_by_xpath('//*[@id="ecc_reporting13_mainPanel_header-maximize"]').click()
    driver.find_element_by_xpath('//*[@id="ecc_reporting14_mainPanel_header-maximize"]').click()
    # time.sleep(5)            '';ljhgfdsaㅋㅌㅊ퓨ㅜㅡ,./
    time.sleep(15)
    source = driver.page_source
    bs = bs4.BeautifulSoup(source, 'html.parser')
    #tools = bs.find_all('div', id_="ecc_reporting13_Main_leftSidebar-placeholder-innerCt")
    #tools = driver.find_elements_by_class_name("x-tool x-box-item x-tool-default x-top x-tool-top x-tool-default-top x-tool-before-title")
    #bar = driver.find_element_by_id("ecc_reporting14_Main_leftSidebar-placeholder-targetEl")
    #barbs = bs4.BeautifulSoup(bar, "html.parser")
    catch1 = bs.findAll("div", id="ecc_reporting14_Main_leftSidebar-placeholder-targetEl")
    strcatch1 = str(catch1)
    strlist1 = strcatch1.split(" ")
    toolId1 = strlist1[12]
    driver.find_element_by_xpath('//*[@'+toolId1+']').click()

    catch2 = bs.findAll("div", id="ecc_reporting14_Main_leftSidebar-innerCt")
    strcatch2 = str(catch2)
    strlist2 = strcatch2.split(" ")
    exportId1 = strlist2[50]
    driver.find_element_by_xpath('//*[@'+exportId1+']').click()

    driver.find_element_by_xpath("//*[contains(text(), 'CSV')]").click()

    tabs = driver.window_handles
    driver.switch_to_window(tabs[0])
    driver.get('https://eadvantage.siemens.com/emc/')
    time.sleep(20)
    driver.switch_to.frame('dashboard-frame')
    time.sleep(10)
    driver.find_element_by_xpath('//*[@id="ecc_reporting13_mainPanel_header-maximize"]').click()
    time.sleep(15)

    source = driver.page_source
    bs = bs4.BeautifulSoup(source, 'html.parser')

    catch3 = bs.findAll("div", id="ecc_reporting13_Main_leftSidebar-placeholder-targetEl")
    strcatch3 = str(catch3)
    strlist3 = strcatch3.split(" ")
    toolId2 = strlist3[12]
    driver.find_element_by_xpath('//*[@'+toolId2+']').click()

    catch4 = bs.findAll("div", id="ecc_reporting13_Main_leftSidebar-innerCt")
    strcatch4 = str(catch4)
    strlist4 = strcatch4.split(" ")
    exportId2 = strlist4[50]
    driver.find_element_by_xpath('//*[@'+exportId2+']').click()
    driver.find_element_by_xpath("//*[contains(text(), 'CSV')]").click()
    driver.close()

if __name__ == "__main__": ## main 함수
    thread_run()